﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DATHelperTool.DatNet {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://www.dat.de/DAT-NET-Webservice", ConfigurationName="DatNet.DatNetWs")]
    public interface DatNetWs {
        
        // CODEGEN: Der Nachrichtenvertrag wird generiert, da der Wrappername (Request) von Nachricht "doAction" nicht mit dem Standardwert (doAction) übereinstimmt.
        [System.ServiceModel.OperationContractAttribute(Action="http://www.dat.de/DAT-NET-Webservice/Action", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(Attachment[]))]
        DATHelperTool.DatNet.doActionResponse doAction(DATHelperTool.DatNet.doAction request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.dat.de/DAT-NET-Webservice/Action", ReplyAction="*")]
        System.Threading.Tasks.Task<DATHelperTool.DatNet.doActionResponse> doActionAsync(DATHelperTool.DatNet.doAction request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3190.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.dat.de/DAT-NET-Webservice")]
    public partial class ActionRequest : object, System.ComponentModel.INotifyPropertyChanged {
        
        private Info infoField;
        
        private Notice noticeField;
        
        private Attachment[] attachmentsField;
        
        private string loginField;
        
        private string passwordField;
        
        private string processKeyField;
        
        private string actionNameField;
        
        private string statusNowField;
        
        private string statusBeforeField;
        
        private System.DateTime actionDateField;
        
        private bool actionDateFieldSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public Info Info {
            get {
                return this.infoField;
            }
            set {
                this.infoField = value;
                this.RaisePropertyChanged("Info");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public Notice Notice {
            get {
                return this.noticeField;
            }
            set {
                this.noticeField = value;
                this.RaisePropertyChanged("Notice");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        [System.Xml.Serialization.XmlArrayItemAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
        public Attachment[] Attachments {
            get {
                return this.attachmentsField;
            }
            set {
                this.attachmentsField = value;
                this.RaisePropertyChanged("Attachments");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string login {
            get {
                return this.loginField;
            }
            set {
                this.loginField = value;
                this.RaisePropertyChanged("login");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string password {
            get {
                return this.passwordField;
            }
            set {
                this.passwordField = value;
                this.RaisePropertyChanged("password");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ProcessKey {
            get {
                return this.processKeyField;
            }
            set {
                this.processKeyField = value;
                this.RaisePropertyChanged("ProcessKey");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ActionName {
            get {
                return this.actionNameField;
            }
            set {
                this.actionNameField = value;
                this.RaisePropertyChanged("ActionName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string StatusNow {
            get {
                return this.statusNowField;
            }
            set {
                this.statusNowField = value;
                this.RaisePropertyChanged("StatusNow");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string StatusBefore {
            get {
                return this.statusBeforeField;
            }
            set {
                this.statusBeforeField = value;
                this.RaisePropertyChanged("StatusBefore");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType="date")]
        public System.DateTime ActionDate {
            get {
                return this.actionDateField;
            }
            set {
                this.actionDateField = value;
                this.RaisePropertyChanged("ActionDate");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionDateSpecified {
            get {
                return this.actionDateFieldSpecified;
            }
            set {
                this.actionDateFieldSpecified = value;
                this.RaisePropertyChanged("ActionDateSpecified");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3190.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.dat.de/DAT-NET-Webservice")]
    public partial class Info : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string declarationOfAssignmentField;
        
        private string preTaxDeductionRightField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DeclarationOfAssignment {
            get {
                return this.declarationOfAssignmentField;
            }
            set {
                this.declarationOfAssignmentField = value;
                this.RaisePropertyChanged("DeclarationOfAssignment");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PreTaxDeductionRight {
            get {
                return this.preTaxDeductionRightField;
            }
            set {
                this.preTaxDeductionRightField = value;
                this.RaisePropertyChanged("PreTaxDeductionRight");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3190.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.dat.de/DAT-NET-Webservice")]
    public partial class ActionResponse : object, System.ComponentModel.INotifyPropertyChanged {
        
        private ActionResponseError errorField;
        
        private string processKeyField;
        
        private string statusNowField;
        
        private string statusBeforeField;
        
        private System.DateTime actionDateField;
        
        private bool actionDateFieldSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public ActionResponseError Error {
            get {
                return this.errorField;
            }
            set {
                this.errorField = value;
                this.RaisePropertyChanged("Error");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ProcessKey {
            get {
                return this.processKeyField;
            }
            set {
                this.processKeyField = value;
                this.RaisePropertyChanged("ProcessKey");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string StatusNow {
            get {
                return this.statusNowField;
            }
            set {
                this.statusNowField = value;
                this.RaisePropertyChanged("StatusNow");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string StatusBefore {
            get {
                return this.statusBeforeField;
            }
            set {
                this.statusBeforeField = value;
                this.RaisePropertyChanged("StatusBefore");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType="date")]
        public System.DateTime ActionDate {
            get {
                return this.actionDateField;
            }
            set {
                this.actionDateField = value;
                this.RaisePropertyChanged("ActionDate");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionDateSpecified {
            get {
                return this.actionDateFieldSpecified;
            }
            set {
                this.actionDateFieldSpecified = value;
                this.RaisePropertyChanged("ActionDateSpecified");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3190.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.dat.de/DAT-NET-Webservice")]
    public partial class ActionResponseError : object, System.ComponentModel.INotifyPropertyChanged {
        
        private Notice errMessageField;
        
        private bool isTechErrField;
        
        private bool isProcessErrField;
        
        private string statusAckField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public Notice ErrMessage {
            get {
                return this.errMessageField;
            }
            set {
                this.errMessageField = value;
                this.RaisePropertyChanged("ErrMessage");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool IsTechErr {
            get {
                return this.isTechErrField;
            }
            set {
                this.isTechErrField = value;
                this.RaisePropertyChanged("IsTechErr");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool IsProcessErr {
            get {
                return this.isProcessErrField;
            }
            set {
                this.isProcessErrField = value;
                this.RaisePropertyChanged("IsProcessErr");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string StatusAck {
            get {
                return this.statusAckField;
            }
            set {
                this.statusAckField = value;
                this.RaisePropertyChanged("StatusAck");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3190.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.dat.de/DAT-NET-Webservice")]
    public partial class Notice : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string headerField;
        
        private string bodyField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string Header {
            get {
                return this.headerField;
            }
            set {
                this.headerField = value;
                this.RaisePropertyChanged("Header");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string Body {
            get {
                return this.bodyField;
            }
            set {
                this.bodyField = value;
                this.RaisePropertyChanged("Body");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3190.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.dat.de/DAT-NET-Webservice")]
    public partial class Attachment : object, System.ComponentModel.INotifyPropertyChanged {
        
        private byte[] fILEField;
        
        private string fileNameField;
        
        private long fileSizeField;
        
        private bool fileSizeFieldSpecified;
        
        private System.DateTime fileDateField;
        
        private bool fileDateFieldSpecified;
        
        private string fileCategoryField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="base64Binary", Order=0)]
        public byte[] FILE {
            get {
                return this.fILEField;
            }
            set {
                this.fILEField = value;
                this.RaisePropertyChanged("FILE");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FileName {
            get {
                return this.fileNameField;
            }
            set {
                this.fileNameField = value;
                this.RaisePropertyChanged("FileName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public long FileSize {
            get {
                return this.fileSizeField;
            }
            set {
                this.fileSizeField = value;
                this.RaisePropertyChanged("FileSize");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FileSizeSpecified {
            get {
                return this.fileSizeFieldSpecified;
            }
            set {
                this.fileSizeFieldSpecified = value;
                this.RaisePropertyChanged("FileSizeSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType="date")]
        public System.DateTime FileDate {
            get {
                return this.fileDateField;
            }
            set {
                this.fileDateField = value;
                this.RaisePropertyChanged("FileDate");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FileDateSpecified {
            get {
                return this.fileDateFieldSpecified;
            }
            set {
                this.fileDateFieldSpecified = value;
                this.RaisePropertyChanged("FileDateSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FileCategory {
            get {
                return this.fileCategoryField;
            }
            set {
                this.fileCategoryField = value;
                this.RaisePropertyChanged("FileCategory");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="Request", WrapperNamespace="http://www.dat.de/DAT-NET-Webservice", IsWrapped=true)]
    public partial class doAction {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.dat.de/DAT-NET-Webservice", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute("ActionRequest", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public DATHelperTool.DatNet.ActionRequest[] ActionRequest;
        
        public doAction() {
        }
        
        public doAction(DATHelperTool.DatNet.ActionRequest[] ActionRequest) {
            this.ActionRequest = ActionRequest;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="Response", WrapperNamespace="http://www.dat.de/DAT-NET-Webservice", IsWrapped=true)]
    public partial class doActionResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.dat.de/DAT-NET-Webservice", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute("ActionResponse", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public DATHelperTool.DatNet.ActionResponse[] ActionResponse;
        
        public doActionResponse() {
        }
        
        public doActionResponse(DATHelperTool.DatNet.ActionResponse[] ActionResponse) {
            this.ActionResponse = ActionResponse;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface DatNetWsChannel : DATHelperTool.DatNet.DatNetWs, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class DatNetWsClient : System.ServiceModel.ClientBase<DATHelperTool.DatNet.DatNetWs>, DATHelperTool.DatNet.DatNetWs {
        
        public DatNetWsClient() {
        }
        
        public DatNetWsClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public DatNetWsClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DatNetWsClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DatNetWsClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        DATHelperTool.DatNet.doActionResponse DATHelperTool.DatNet.DatNetWs.doAction(DATHelperTool.DatNet.doAction request) {
            return base.Channel.doAction(request);
        }
        
        public DATHelperTool.DatNet.ActionResponse[] doAction(DATHelperTool.DatNet.ActionRequest[] ActionRequest) {
            DATHelperTool.DatNet.doAction inValue = new DATHelperTool.DatNet.doAction();
            inValue.ActionRequest = ActionRequest;
            DATHelperTool.DatNet.doActionResponse retVal = ((DATHelperTool.DatNet.DatNetWs)(this)).doAction(inValue);
            return retVal.ActionResponse;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<DATHelperTool.DatNet.doActionResponse> DATHelperTool.DatNet.DatNetWs.doActionAsync(DATHelperTool.DatNet.doAction request) {
            return base.Channel.doActionAsync(request);
        }
        
        public System.Threading.Tasks.Task<DATHelperTool.DatNet.doActionResponse> doActionAsync(DATHelperTool.DatNet.ActionRequest[] ActionRequest) {
            DATHelperTool.DatNet.doAction inValue = new DATHelperTool.DatNet.doAction();
            inValue.ActionRequest = ActionRequest;
            return ((DATHelperTool.DatNet.DatNetWs)(this)).doActionAsync(inValue);
        }
    }
}
