﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DATHelperTool.DAT;
using DATHelperTool.Authentication;
using System.Security.Cryptography;
using System.Net.Http;
using System.Xml;
using System.Net;
using System.IO;
using DATHelperTool.VehicleIdentificationService;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text.RegularExpressions;

namespace DATHelperTool
{
    // Nested Namepspace
    namespace DAT
    {
        class DAT
        {
            public void SampleMethod()
            {
                System.Console.WriteLine(
                  "SampleMethod inside NestedNamespace");
            }

            //public async Task<string> CreateSoapEnvelope()
            //{
            //    string soapString = @"<?xml version=""1.0"" encoding=""utf-8""?>
            //        <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
            //        <soap:Header/>
            //          <soap:Body>
            //            <eval:>
            //            <HelloWorld xmlns=""http://tempuri.org/"" />
            //          </soap:Body>
            //        </soap:Envelope>";

            //    HttpResponseMessage response = await PostXmlRequest("your_url_here", soapString);
            //    string content = await response.Content.ReadAsStringAsync();

            //    return content;
            //}

            public static async Task<HttpResponseMessage> PostXmlRequest(string baseUrl, string xmlString)
            {
                using (var httpClient = new HttpClient())
                {
                    var httpContent = new StringContent(xmlString, Encoding.UTF8, "text/xml");
                    httpContent.Headers.Add("SOAPAction", "http://tempuri.org/HelloWorld");

                    return await httpClient.PostAsync(baseUrl, httpContent);
                }
            }


            public static void CallWebService()
            {
                var _url = "http://www.dat.de/valuateNG/soap/Authentication";
                var _action = "http://www.dat.de/valuateNG/soap/Authentication?op=HelloWorld";

                XmlDocument soapEnvelopeXml = CreateSoapEnvelope();
                HttpWebRequest webRequest = CreateWebRequest(_url, _action);
                InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

                // begin async call to web request.
                IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

                // suspend this thread until call is complete. You might want to
                // do something usefull here like update your UI.
                asyncResult.AsyncWaitHandle.WaitOne();

                // get the response from the completed web request.
                string soapResult;
                using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
                {
                    using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                    {
                        soapResult = rd.ReadToEnd();
                    }
                    Console.Write(soapResult);
                }
            }

            private static HttpWebRequest CreateWebRequest(string url, string action)
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Headers.Add("SOAPAction", action);
                webRequest.ContentType = "text/xml;charset=\"utf-8\"";
                webRequest.Accept = "text/xml";
                webRequest.Method = "POST";
                return webRequest;
            }

            private static XmlDocument CreateSoapEnvelope()
            {
                XmlDocument soapEnvelopeDocument = new XmlDocument();
                soapEnvelopeDocument.LoadXml(@"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/1999/XMLSchema""><SOAP-ENV:Body><HelloWorld xmlns=""http://tempuri.org/"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><int1 xsi:type=""xsd:integer"">12</int1><int2 xsi:type=""xsd:integer"">32</int2></HelloWorld></SOAP-ENV:Body></SOAP-ENV:Envelope>");
                return soapEnvelopeDocument;
            }

            private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
            {
                using (Stream stream = webRequest.GetRequestStream())
                {
                    soapEnvelopeXml.Save(stream);
                }
            }


        }
    }

    public class SHA256
    {
        public static string MakeHashSHA256(string INPUT_DATA)
        {
            var StringInBytes = System.Text.Encoding.UTF8.GetBytes(INPUT_DATA);
            string BytesInString;
            using (System.Security.Cryptography.SHA256 shaM = new SHA256Managed())
            {
                //byte[] data = new byte[StringBytes];
                byte[] result = null;
                //SHA512 shaM = new SHA512Managed();
                result = shaM.ComputeHash(StringInBytes);
                try
                {
                    BytesInString = GetStringFromHash(result);
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return BytesInString;
        }

        /// <summary>
        /// Private Method wich converts a byte array back to a string
        /// </summary>
        /// <param name="HASH">The hash as byte array</param>
        /// <returns>The hash a string</returns>
        private static string GetStringFromHash(byte[] HASH)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < HASH.Length; i++)
            {
                result.Append(HASH[i].ToString("X2"));
            }
            return result.ToString();
        }

    }

    public class Program
    {
        public static releaseRestriction ALL { get; private set; }

        static void Main(string[] args)
        {
            DAT.DAT datHelper = new DAT.DAT();

            string DATSessionID ="";

            datHelper.SampleMethod();
            // DAT 1333830:heldseba:heldseba01

            // Kundennummer 1333830
            // Anmeldename heldseba
            // Customer Signature SHA256 9A97C42AF4E7BF571FFB546BB4558BAC4EAE9A483DFB43D5C383EE4BF500B32D
            // Interface Signature SHA256  8DDF603777B2D44855CB990743EFECD68317C3968F45823CC5AC637F817ED654

            // Console.WriteLine(DATHelperTool.SHA256.MakeHashSHA256("1333830:heldseba:heldseba01"));
            Console.ReadLine();

            //Authentication.Authentication auth = new Authentication.Authentication();
            //auth.doLogin();

            //var client = new Authentication.AuthenticationClient();

            //Authentication.doLoginRequest doLoginRequest = new doLoginRequest
            //{
            //    customerLogin = "heldseba",
            //    customerNumber = "1333830",
            //    customerSignature = "9A97C42AF4E7BF571FFB546BB4558BAC4EAE9A483DFB43D5C383EE4BF500B32D",
            //    interfacePartnerNumber = "1333830",
            //    interfacePartnerSignature = "8DDF603777B2D44855CB990743EFECD68317C3968F45823CC5AC637F817ED654"
            //};

            //DATSessionID = client.doLogin(doLoginRequest);
            //Console.WriteLine("Aktuelle DAT SessionID :" + DATSessionID);

            var clientCarIdenfication = new VehicleIdentificationService.VehicleIdentificationServiceClient();

            // Creating a HTTP Header for authentication against DAT
            using (new OperationContextScope (clientCarIdenfication.InnerChannel))
            {
                HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                requestMessage.Headers["customerLogin"] = "heldseba";
                requestMessage.Headers["customerNumber"] = "1333830";
                requestMessage.Headers["customerSignature"] = "9A97C42AF4E7BF571FFB546BB4558BAC4EAE9A483DFB43D5C383EE4BF500B32D";
                requestMessage.Headers["interfacePartnerNumber"] = "1333830";
                requestMessage.Headers["interfacePartnerSignature"] = "8DDF603777B2D44855CB990743EFECD68317C3968F45823CC5AC637F817ED654";

                OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;

               

                VehicleIdentificationService.locale localeDAT = new VehicleIdentificationService.locale
                {
                    country = "DE",
                    datCountryIndicator = "DE",
                    language = "de"
                };

                VehicleIdentificationService.vinSelectionRequest vinSelectionRequest = new VehicleIdentificationService.vinSelectionRequest
                {
                    //vin = "WB1DEXTESTSTUB001",  // Test VIN DAT - BMW Motorrad
                    //vin = "WBADEXTESTSTUB001",  // Test-VIN DAT - BMW 5er F11
                    //vin = "WF0DEXTESTSTUB001",   // Test-VIN DAT >64k - Ford Focus Lim.
                    vin = "MALDEXTESTSTUB001" , // Test-VIN DAT - hyundai i10 Edition +
                    //vin = "JTHDEXTESTSTUB001", // Lexus GS 450h
                    //vin = "WMADEXTESTSTUB001", // LKW MAN TGS > 64k
                    restriction = ALL,
                    constructionTime = 0,
                    locale = localeDAT
                };

                var test = clientCarIdenfication.getVehicleIdentificationByVin(vinSelectionRequest);

                Console.WriteLine(test.ToString());
                Console.WriteLine("BaseModelName : " + Regex.Match(test.Dossier[0].Vehicle.BaseModelName.Value, @"\(.*"));
                Console.WriteLine("SubModelName : " + test.Dossier[0].Vehicle.SubModelName.Value);
               
            }

            Console.ReadLine();
        }


    }


}
